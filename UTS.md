## 1. Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. (Lampirkan link source code terkait)
## 2. Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)
## 3. Mampu menjelaskan konsep dasar OOP
## 4. Mampu mendemonstrasikan penggunaan Encapsulation secara tepat  (Lampirkan link source code terkait)
## 5. Mampu mendemonstrasikan penggunaan Abstraction secara tepat  (Lampirkan link source code terkait)
## 6. Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat  (Lampirkan link source code terkait)
## 7. Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat  (Lampirkan link source code terkait)
## 8. Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table  (Lampirkan diagram terkait)
# Diagram Class
```mermaid 
classDiagram 
    class telkomsel {
        +menu()
    }
    
    class promo {
        +promo()
    }
    
    class info {
        +menu()
        +promo()
        +registrasi()
    }
    
    class Pembelian {
        -dana: double
        -ovo: double
        -hargaPulsa: double
        -hargaPaketData: double
        -totalpulsa: double
        -totaldata: double
        +Pembelian(ovo: double, dana: double, hargaPulsa: double, hargaPaketData: double, totalpulsa: double, totaldata: double)
        +PulsaDana(jumlah: double)
        +PulsaOvo(jumlah: double)
        +DataDana(jumlah: double)
        +DataOvo(jumlah: double)
        +datapulsa(jumlah: double)
        +info()
    }
    
    class Metode {
        -nomor: String
        -sandi: String
        +setnomor(nomor: String)
        +setsandi(sandi: String)
        +getnomor(): String
        +getsandi(): String
    }
    
    class Dana {
        +masukkannomor(nomor: String)
        +masukkansandi(sandi: String)
    }
    
    class Ovo {
        +masukkannomor(nomor: String)
        +masukkansandi(sandi: String)
    }
    
    class Telkomtsel {
        +main(args: String[])
    }
    
    telkomsel <|-- info
    promo <|.. info
    Pembelian o-- Metode
    Dana <|.. Metode
    Ovo <|.. Metode
    Telkomtsel --> info

```
# Use case diagram

| nomor | pengguna | use case | prioritas |
| - | ------ | ------ | ------ |
| 1 |   User     |    membeli pulsa    |    100    |
| 2 |   User     |    membeli data    |    100    |
| 3 |   User     |    mengecek sisa pulsa    |    60    |
| 4 |    User    |    mengecek sisa paket data    |    60    |
| 5 |    User    |    login    |    35    |
| 6 |    User    |    registrasi    |    40    |
| 7 |    user    |    Memasukkan nickname    |    5    |
| 8 |    User    |    memperoleh poin dari setiap aktivitas    |    30    |
| 9 |     User   |    Check-in harian    |    15    |
| 10 |     User   |    melihat riwayat    |    40    |
| 11 |   User     |    menambah nomor    |    10    |
| 12 |   User     |    mengirim hadiah    |    25    |
| 13 |    User    |    memberi masukan    |    65    |
| 14 |    User    |    Menghubungi customer service    |    80    |
| 15 |    User    |    menukar poin    |    20    |
| 16 |    User    |    melihat masa aktif  nomor   |    80    |
| 17 |    user    |    pindah ke kartu lain sesama provider    |    55    |
| 18 |    User    |    melihat ping internet    |    35    |
| 19 |    User    |    mengganti bahasa    |    25    |
| 20 |    User    |    melakukan pembayaran via pulsa    |    100    |
| 21 |   User     |    membeli pulsa    |    100    |
| 22 |   User     |    membeli data    |    100    |
| 23 |    User    |    berlangganan    |    15    |
| 24 |    User    |    melakukan paylater    |    75    |
| 25 |    User    |    paket roaming    |    90    |
| 26 |    User    |    aktivasi 5G    |    85    |
| 27 |    User    |    membeli paket 5G    |    90    |
| 28 |    User    |    membayar token listrik    |    45    |
| 29 |    User    |    membayar indihome    |    50    |
| 30 |    User    |    melihat lokasi Grapari    |    5    |
| 31 |   User     |    mengunjungi Grapari Online    |    25    |
| 32 |   User     |    pembayaran melalui e-wallet    |    100    |
| 33 |    User    |    pembayaran melalui e-banking    |    85    |
| 34 |    User    |    mengunci pulsa untuk transaksi    |    60    |
| 35 |    User    |    transaksi dengan voucher    |  40      |
| 36 |   User     |    Menghubungkan dengan e-wallet    |     85   |
| 37 |    User    |   Menghubungkan dengan e-banking     |    80    |
| 38 |    User    |    melihat saldo e-wallet    |    60    |
| 39 |    User    |    melihat sisa saldo e-banking    |    60    |
| 40 |     User   |    melihat iklan    |    50    |
| 41 |   User     |    pembayaran Qris    |    85    |
| 42 |   User     |    membeli data    |    100    |
| 43 |    User    |    melihat status prabayar    |    20    |
| 44 |    User    |     memberi nilai   |    10    |
| 45 |     User   |    membeli paket gaming    |    100    |
| 46 |    User    |     membeli paket pendidikan   |    100    |
| 47 |    User    |    membeli paket sosial media    |   100     |
| 48 |    User    |    mengaktifkan NSP    |    65    |
| 49 |    User    |    menelusuri lagu NSP terpopuler    |    40    |
| 50 |    User    |    membeli paket kesehatan    |    100    |
| 51 |   User     |    membeli paket darurat    |    100    |
| 52 |   User     |    membeli notifikasi telkomsel    |    100    |
| 53 |    User    |    membeli paket bulanan    |    100    |
| 54 |    User    |    membeli paket mingguan    |    100    |
| 55 |    user    |     membeli paket harian   |    100    |
| 56 |    User    |    membeli paket malam    |    100    |
| 57 |    User    |    mengikuti undian    |    25    |
| 58 |     User   |    membeli produk    |    65    |
| 59 |     User   |    membeli paket telepon    |    100    |
| 60 |    User    |    membeli paket SMS    |    100    |
| 61 |   User     |    berlangganan modem orbit    |    85    |
| 62 |   User     |    mengajukan keluhan    |    70    |
| 63 |    User    |    berlangganan aplikasi streaming    |    65    |
| 64 |    User    |    menyimpan video di CloudMAX    |    40    |
| 65 |    User    |    menyimpan foto di CloudMAX    |    40    |
| 66 |     User   |    menyimpan dokumen di CloudMAX    |    40    |
| 67 |    User    |    menjelajahi video pendek    |    20    |
| 68 |    User    |    menonton film    |    20    |
| 69 |     User   |    mengubah suara ketika telepon    |    25    |
| 70 |     User   |    menambah backssound ketika telepon    |    25    |
| 71 |   User     |    melakukan collect call    |    10    |
| 72 |   User     |    membeli paket streaming    |    85    |
| 73 |    User    |    dapat berdonasi    |    30    |
| 74 |    User    |    melihat voucher terpopuler    |    15    |
| 75 |    User    |    melihat voucher terdekat    |    15    |
| 76 |    User    |    melihat voucher yang direkomendasikan    |    15    |
| 77 |    User    |    membeli paket khusus e-wallet    |    70    |
| 78 |    User    |    mengaktifkan layanan volte    |    80    |
| 79 |    User    |    melihat lokasi 5G    |    15    |
| 80 |    User    |    membeli kuota keluarga    |    85    |
| 81 |   User     |    membeli paket perpanjang masa    |    95    |
| 82 |   User     |    mengikuti asuransi    |    25    |
| 83 |    User    |    mengikuti bootcamp    |    25    |
| 84 |     User   |    mengikuti kuis    |    25    |
| 85 |    User    |    mengikuti survey    |    85    |
| 86 |    User    |    membayarkan tagihan    |    85    |
| 87 |    User    |    membelikan pulsa    |    85    |
| 88 |    User    |    membelikan paket data    |    85    |
| 89 |     User   |    melihat rincian voucher    |   25     |
| 90 |    User    |    melihat penawaran spesial berdasarkan langganan    |    45    |
| 91 |   User     |    mengatur parameter dalam pembelian paket    |    25    |
| 92 |   User     |    menggunakan filter untuk rincian produk    |    30   |
| 93 |    Admin    |    melihat data pengguna    |    65    |
| 94 |    Admin    |    mengolah data    |    90    |
| 95 |    Admin    |    melihat ulasan    |    85    |
| 96 |    Admin    |    memblokir pengguna    |    75    |
| 97 |    Admin    |    memverifikasi data pengguna    |    85    |
| 98 |    Admin    |    mengawasi pengguna    |    65    |
| 99 |    Admin    |    membuat keamanan    |    100    |
| 100 |    Admin    |    menampung data    |    100    |

## 9. Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video   (Lampirkan link Youtube terkait)
# penjelasan terkait produk
[penjelasan](https://youtu.be/isMLXvWrSnE)
## 10. Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)

## aplikasi 
[Telkomsel](https://play.google.com/store/apps/details?id=com.telkomsel.telkomselcm)

## penjelasan OOP 
[Link](https://www.youtube.com/watch?v=V1nucocieM0&t=54s)




## diagram class telkomsel
![Diagram](https://gitlab.com/evandika998/praktikum-pemrograman-berorientasi-objek/-/raw/main/Cuplikan_layar_2023-05-12_071703.png)



```java


import java.util.*;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Hashtable;

/*membuat abstrak dan menmasukkan methode menu di dalamnya*/
abstract class telkomsel {
    abstract void menu();
}

interface promo {
    public void promo();
}

/*
 * membuat methode registrasi dengan turunan dari abstrak class sehingga membuat
 * inheritance
 */
class info extends telkomsel implements promo {
    /* polymorphism */
    public void menu() {
        System.out.println("===============================");
        System.out.println("SELAMAT DATANG DI TELKOMSEL");
        System.out.println("===============================");
        System.out.println("1. BELI PULSA/KUOTA");
        System.out.println("2. KRITIK DAN SARAN ");
        System.out.println("3. PROMO ");
        System.out.println("===============================");
    }

    public void promo() {
        Hashtable<String, Integer> Promo = new Hashtable<String, Integer>();

        Promo.put("1. 30 GB/1 bulan = 120000", 1);
        Promo.put("2. 25 GB/1 bulan = 100000", 2);
        Promo.put("3. 15 GB/1 bulan = 90000", 3);
        Promo.put("4. 10 GB/1 bulan = 70000", 4);
        Promo.put("5. 10 GB/1 minggu = 120000", 5);
        Promo.put("6. 8 GB/1 minggu = 50000", 6);
        Promo.put("7. 5 GB/5 hari = 20000", 7);
        Promo.put("8. 2 GB/3 hari = 10000", 8);
        Promo.put("9. 1 GB/1 hari = 5000", 9);
        Promo.put("10. 500 MB/1 hari = 2500", 10);

    }

    public void registrasi() {
        Scanner input2 = new Scanner(System.in);

        System.out.println("Selamat datang di Aplikasi Telkomsel");
        System.out.println("Silakan lengkapi data berikut untuk melakukan registrasi:");

        System.out.print("Nickname: ");
        String nama = input2.nextLine();

        System.out.println("Masukkan nomor telepon:");
        String phoneNumber = input2.nextLine();
        Pattern pattern = Pattern.compile("^(\\+62|0)[2-9][0-9]{8,10}$");
        Matcher matcher = pattern.matcher(phoneNumber);
        if (matcher.matches()) {
            System.out.println("Nomor telepon valid.");
        } else {
            System.out.println("Nomor telepon tidak valid.");
        }

        System.out.print("Email: ");
        String email = input2.nextLine();

        System.out.print("Kata sandi: ");
        String kataSandi = input2.nextLine();

        System.out.print("Ulangi kata sandi: ");
        String ulangiKataSandi = input2.nextLine();

        if (kataSandi.equals(ulangiKataSandi)) {
            System.out.println("Registrasi berhasil!");
            System.out.println("Terima kasih " + nama + " telah bergabung dengan Aplikasi Telkomsel.");
        } else {
            System.out.println("Kata sandi yang Anda masukkan tidak sama.");
            System.exit(0);
        }
    }
}

class Pembelian {
    double saldo;
    double hargaPulsa;
    double hargaPaketData;
    double totalpulsa;
    double totaldata;

    public Pembelian(double saldo, double hargaPulsa, double hargaPaketData, double totalpulsa, double totaldata) {
        this.saldo = saldo;
        this.hargaPulsa = hargaPulsa;
        this.hargaPaketData = hargaPaketData;
        this.totalpulsa = totalpulsa;
        this.totaldata = totaldata;
    }

    public void beliPulsa(double jumlah) {
        double totalHarga = jumlah + 2000;
        if (totalHarga > saldo) {
            System.out.println("Saldo tidak mencukupi.");
        } else {
            saldo -= totalHarga;
            System.out.println("Pulsa sebesar " + jumlah + " berhasil dibeli.");
            System.out.println("Sisa saldo: " + saldo);
            totalpulsa += jumlah;
        }
    }

    public void beliPaketData(double jumlah) {
        double totalHarga = jumlah * 1000 + 3000;
        if (totalHarga > saldo) {
            System.out.println("Saldo tidak mencukupi.");
        } else {
            saldo -= totalHarga;
            System.out.println("Paket data sebesar " + jumlah + " GB berhasil dibeli.");
            System.out.println("Sisa saldo: " + saldo);
            totaldata += jumlah;
        }
    }

    public void info() {
        System.out.println("sisa uang = " + saldo);
        System.out.println("jumlah pulsa saat ini = " + totalpulsa);
        System.out.println("jumlah paket data saat ini = " + totaldata);
    }
}

/* inheritance dan ekapsulasi */
abstract class komen {
    private String kata;
    private int nilai;

    public void setKata(String kata) {
        this.kata = kata;
    }

    public void setnilai(int nilai) {
        this.nilai = nilai;
    }

    public String getkata() {
        return kata;
    }

    public double getnilai() {
        return nilai;
    }

    public void beriSaran(String saran) {
    };

    public void beriRating(int rating) {
    };

}

/* polymorphism */
class Menilai extends komen {
    @Override
    public void beriRating(int rating) {
        setnilai(rating);
        System.out.println("Rating untuk Telkomsel " + getnilai() + ": ");
    }

    public void beriSaran(String saran) {
        setKata(saran);
        System.out.println("Masukkan Saran " + getkata() + ": ");
    }
}

class Review extends komen {
    @Override
    public void beriRating(int rating) {
        setnilai(rating);
        System.out.println("Review  " + getnilai() + ": ");
    }

    public void beriSaran(String saran) {
        setKata(saran);
        System.out.println("Nilai " + getkata() + ": ");
    }
}

class Telkomtsel {
    public static void main(String[] args) {
        info telko = new info();
        Scanner input = new Scanner(System.in);
        Menilai menilai = new Menilai();
        Review review = new Review();

        double saldo = 50000;
        double hargaPulsa = 1000;
        double hargaPaketData = 5000;
        double totalpulsa = 100000;
        double totaldata = 23;
        Pembelian produk = new Pembelian(saldo, hargaPulsa, hargaPaketData, totalpulsa, totaldata);
        char ulg = 'Y';
        telko.registrasi();
        do {
            telko.menu();
            produk.info();
            System.out.println("masukkan opsi   : ");
            int opsi = input.nextInt();
            if (opsi == 1) {
                System.out.println("1. Pulsa \n2. Paket Data");
                int ops = input.nextInt();
                if (ops == 1) {
                    System.out.println("Masukkan jumlah pulsa yang diinginkan   : ");
                    System.out.println("1. 10 k\t 12 ribu");
                    System.out.println("2. 20 k\t 22 ribu");
                    System.out.println("3. 25 k\t 27 ribu");
                    System.out.println("4. 50 k\t 52 ribu");
                    System.out.println("5. 75 k\t 77 ribu");
                    System.out.println("6. 100 k\t 102 ribu");
                    System.out.println("7. 150 k\t 152 ribu");
                    int milih = input.nextInt();
                    switch (milih)
                    {
                        case 1:
                        produk.beliPulsa(10);
                        produk.info();
                        break;

                        case 2:
                        produk.beliPulsa(20);
                        produk.info();
                        break;

                        case 3:
                        produk.beliPulsa(25);
                        produk.info();
                        break;

                        case 4:
                        produk.beliPulsa(50);
                        produk.info();
                        break;

                        case 5:
                        produk.beliPulsa(75);
                        produk.info();
                        break;

                        case 6:
                        produk.beliPulsa(100);
                        produk.info();
                        break;

                        case 7:
                        produk.beliPulsa(150);
                        produk.info();
                        break;

                        default:
                        System.out.println("sistem error");
                        System.exit(0);
                    }
                } else if (ops == 2) {
                    System.out.println("Masukkan paket data yang diinginkan   : ");
                    System.out.println("1. 1 GB\t 3 ribu");
                    System.out.println("2. 2 GB\t 5 ribu");
                    System.out.println("3. 5 GB\t 8 ribu");
                    System.out.println("4. 10 GB\t 13 ribu");
                    System.out.println("5. 20 GB\t 23 ribu");
                    System.out.println("6. 50 GB\t 53 ribu");
                    System.out.println("7. 100 GB\t 103 ribu");
                    int milih = input.nextInt();
                    switch (milih)
                    {
                        case 1:
                        produk.beliPaketData(1);
                        produk.info();
                        break;

                        case 2:
                        produk.beliPaketData(2);
                        produk.info();
                        break;

                        case 3:
                        produk.beliPaketData(5);
                        produk.info();
                        break;

                        case 4:
                        produk.beliPaketData(10);
                        produk.info();
                        break;

                        case 5:
                        produk.beliPaketData(20);
                        produk.info();
                        break;

                        case 6:
                        produk.beliPaketData(50);
                        produk.info();
                        break;

                        case 7:
                        produk.beliPaketData(100);
                        produk.info();
                        break;

                        default:
                        System.out.println("sistem error");
                        System.exit(0);
                    }
                }
            } else if (opsi == 2) {
                System.out.println("1. Ulasan\n2. Review");
                int opsii = input.nextInt();
                if (opsii == 1) {
                    System.out.println("Masukkan nilai (0/5) :");
                    menilai.setnilai(input.nextInt());
                    System.out.println("Masukkan masukan :");
                    menilai.setKata(input.nextLine());
                } else if (opsii == 2) {
                    System.out.println("Masukkan nilai untuk review (0/5) :");
                    review.setnilai(input.nextInt());
                    System.out.println("Masukkan saran review : ");
                    review.setKata(input.nextLine());
                }
            } else if (opsi == 3) {
                telko.promo();
            } else {
                System.out.println("error");
            }
            System.out.println("Ingin Mengulang: \nY\tN");
            ulg = input.next().charAt(0);
        } while (ulg == 'Y');
    }
}```



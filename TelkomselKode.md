## Kode
```Java

import java.util.*;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Hashtable;

/*membuat abstrak dan menmasukkan methode menu di dalamnya*/
abstract class telkomsel {
    abstract void menu();
}

interface promo {
    public void promo();
}

/*
 * membuat methode registrasi dengan turunan dari abstrak class sehingga membuat
 * inheritance
 */
class info extends telkomsel implements promo {
    /* polymorphism */
    public void menu() {
        System.out.println("===============================");
        System.out.println("SELAMAT DATANG DI TELKOMSEL");
        System.out.println("===============================");
        System.out.println("1. BELI PULSA/KUOTA");
        System.out.println("2. KRITIK DAN SARAN ");
        System.out.println("3. PROMO ");
        System.out.println("===============================");
    }

    public void promo() {
        Hashtable<String, Integer> Promo = new Hashtable<String, Integer>();

        Promo.put("1. 30 GB/1 bulan = 120000", 1);
        Promo.put("2. 25 GB/1 bulan = 100000", 2);
        Promo.put("3. 15 GB/1 bulan = 90000", 3);
        Promo.put("4. 10 GB/1 bulan = 70000", 4);
        Promo.put("5. 10 GB/1 minggu = 120000", 5);
        Promo.put("6. 8 GB/1 minggu = 50000", 6);
        Promo.put("7. 5 GB/5 hari = 20000", 7);
        Promo.put("8. 2 GB/3 hari = 10000", 8);
        Promo.put("9. 1 GB/1 hari = 5000", 9);
        Promo.put("10. 500 MB/1 hari = 2500", 10);

    }

    public void registrasi() {
        Scanner input2 = new Scanner(System.in);

        System.out.println("Selamat datang di Aplikasi Telkomsel");
        System.out.println("Silakan lengkapi data berikut untuk melakukan registrasi:");

        System.out.print("Nickname: ");
        String nama = input2.nextLine();

        System.out.println("Masukkan nomor telepon:");
        String nomor_telepon = input2.nextLine();
        Pattern pattern = Pattern.compile("^(\\+62|0)[2-9][0-9]{8,10}$");
        Matcher matcher = pattern.matcher(nomor_telepon);
        if (matcher.matches()) {
            System.out.println("Nomor telepon valid.");
        } else {
            System.out.println("Nomor telepon tidak valid.");
        }

        System.out.print("Email: ");
        String email = input2.nextLine();

        System.out.print("Kata sandi: ");
        String kataSandi = input2.nextLine();

        System.out.print("Ulangi kata sandi: ");
        String ulangiKataSandi = input2.nextLine();

        if (kataSandi.equals(ulangiKataSandi)) {
            System.out.println("Registrasi berhasil!");
            System.out.println("Terima kasih " + nama + " telah bergabung dengan Aplikasi Telkomsel.");
        } else {
            System.out.println("Kata sandi yang Anda masukkan tidak sama.");
            System.exit(0);
        }
    }
}

class Pembelian {
    double dana;
    double ovo;
    double hargaPulsa;
    double hargaPaketData;
    double totalpulsa;
    double totaldata;

    public Pembelian(double ovo, double dana, double hargaPulsa, double hargaPaketData, double totalpulsa,
            double totaldata) {
        this.dana = dana;
        this.ovo = ovo;
        this.hargaPulsa = hargaPulsa;
        this.hargaPaketData = hargaPaketData;
        this.totalpulsa = totalpulsa;
        this.totaldata = totaldata;
    }

    public void PulsaDana(double jumlah) {
        double totalHarga = jumlah + 2000;
        if (totalHarga > dana) {
            System.out.println("Saldo tidak mencukupi.");
        } else {
            dana -= totalHarga;
            System.out.println("Pulsa sebesar " + jumlah + " berhasil dibeli.");
            System.out.println("Sisa saldo: " + dana);
            totalpulsa += jumlah;
        }
    }

    public void PulsaOvo(double jumlah) {
        double totalHarga = jumlah + 2000;
        if (totalHarga > ovo) {
            System.out.println("Saldo tidak mencukupi.");
        } else {
            ovo -= totalHarga;
            System.out.println("Pulsa sebesar " + jumlah + " berhasil dibeli.");
            System.out.println("Sisa saldo: " + ovo);
            totalpulsa += jumlah;
        }
    }

    public void DataDana(double jumlah) {
        double totalHarga = jumlah * 1000 + 3000;
        if (totalHarga > dana) {
            System.out.println("Saldo tidak mencukupi.");
        } else {
            dana -= totalHarga;
            System.out.println("Paket data sebesar " + jumlah + " GB berhasil dibeli.");
            System.out.println("Sisa saldo: " + dana);
            totaldata += jumlah;
        }
    }

    public void DataOvo(double jumlah) {
        double totalHarga = jumlah * 1000 + 3000;
        if (totalHarga > ovo) {
            System.out.println("Saldo tidak mencukupi.");
        } else {
            ovo -= totalHarga;
            System.out.println("Paket data sebesar " + jumlah + " GB berhasil dibeli.");
            System.out.println("Sisa saldo: " + ovo);
            totaldata += jumlah;
        }
    }

    public void datapulsa(double jumlah) {
        double totalHarga = jumlah * 1000 + 3000;
        if (totalHarga > totalpulsa) {
            System.out.println("Saldo tidak mencukupi.");
        } else {
            System.out.println("Paket data sebesar " + jumlah + " GB berhasil dibeli.");
            totaldata += jumlah;
            totalpulsa -= totalHarga;
        }
    }

    public void info() {
        System.out.println("jumlah pulsa saat ini = " + totalpulsa);
        System.out.println("jumlah paket data saat ini = " + totaldata);
    }
}

/* inheritance dan ekapsulasi */
abstract class Metode {
    private String nomor;
    private String sandi;

    public void setnomor(String nomor) {
        this.nomor = nomor;
    }

    public void setsandi(String sandi) {
        this.sandi = sandi;
    }

    public String getnomor() {
        return nomor;
    }

    public String getsandi() {
        return sandi;
    }

    public void masukkannomor(String nomor) {

    };

    public void masukkansandi(String sandi) {

    };
}

/* polymorphism */
class Dana extends Metode {
    @Override
    public void masukkannomor(String nomor) {
        setnomor(nomor);
        System.out.println("Nomor Dana kamu = " + getnomor() + ": ");
    }

    public void masukkansandi(String sandi) {
        System.out.println("Masukkan Sandi");
        setsandi(sandi);
        System.out.println("Sandi : " + getsandi() + ": ");
    }
}

class Ovo extends Metode {
    @Override
    public void masukkannomor(String nomor) {
        setnomor(nomor);
        System.out.println("Nomor OVO kamu = " + getnomor() + ": ");
    }

    public void masukkansandi(String sandi) {
        setsandi(sandi);
        System.out.println("Sandi : " + getsandi() + ": ");
    }
}

class Telkomtsel {
    public static void main(String[] args) {
        info telko = new info();
        Scanner input = new Scanner(System.in);
        Dana Dana = new Dana();
        Ovo OVO = new Ovo();
        Scanner inputan = new Scanner(System.in);

        double dana = 50000;
        double ovo = 100000;
        double hargaPulsa = 1000;
        double hargaPaketData = 5000;
        double totalpulsa = 100000;
        double totaldata = 23;
        Pembelian produk = new Pembelian(ovo, dana, hargaPulsa, hargaPaketData, totalpulsa, totaldata);
        char ulg = 'Y';
        telko.registrasi();
        do {
            telko.menu();
            produk.info();
            System.out.println("masukkan opsi   : ");
            int opsi = input.nextInt();
            if (opsi == 1) {
                System.out.println("=======\n1. Pulsa \n2. Paket Data\n=======");
                int ops = input.nextInt();

                if (ops == 1) {
                    System.out.println("Masukkan jumlah pulsa yang diinginkan   : ");
                    System.out.println("1. 10 k\t 12 ribu");
                    System.out.println("2. 20 k\t 22 ribu");
                    System.out.println("3. 25 k\t 27 ribu");
                    System.out.println("4. 50 k\t 52 ribu");
                    System.out.println("5. 75 k\t 77 ribu");
                    System.out.println("6. 100 k\t 102 ribu");
                    System.out.println("7. 150 k\t 152 ribu");
                    int pilih;
                    int milih = input.nextInt();
                    switch (milih) {
                        case 1:
                            System.out.println("Masukkan Metode Pembayaran :\n1. Dana\n2. OVO");
                            pilih = input.nextInt();
                            if (pilih == 1) {
                                System.out.println("Masukkan nomor Dana : ");
                                Dana.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Kata Sandi : ");
                                Dana.setsandi(inputan.nextLine());
                                produk.PulsaDana(10000);
                            } else if (pilih == 2) {
                                System.out.println("Masukkan Nomor OVO : ");
                                OVO.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Sandi : ");
                                OVO.setsandi(inputan.nextLine());
                                produk.PulsaOvo(10000);
                            }
                            produk.info();
                            break;

                        case 2:
                            System.out.println("Masukkan Metode Pembayaran :\n1. Dana\n2. OVO");
                            pilih = input.nextInt();
                            if (pilih == 1) {
                                System.out.println("Masukkan nomor Dana : ");
                                Dana.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Kata Sandi : ");
                                Dana.setsandi(inputan.nextLine());
                                produk.PulsaDana(20000);
                            } else if (pilih == 2) {
                                System.out.println("Masukkan Nomor OVO : ");
                                OVO.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Sandi : ");
                                OVO.setsandi(inputan.nextLine());
                                produk.PulsaOvo(20000);
                            }
                            produk.info();
                            break;

                        case 3:
                            System.out.println("Masukkan Metode Pembayaran :\n1. Dana\n2. OVO");
                            pilih = input.nextInt();
                            if (pilih == 1) {
                                System.out.println("Masukkan nomor Dana : ");
                                Dana.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Kata Sandi : ");
                                Dana.setsandi(inputan.nextLine());
                                produk.PulsaDana(25000);
                            } else if (pilih == 2) {
                                System.out.println("Masukkan Nomor OVO : ");
                                OVO.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Sandi : ");
                                OVO.setsandi(inputan.nextLine());
                                produk.PulsaOvo(25000);
                            }
                            produk.info();
                            break;
                        case 4:
                            System.out.println("Masukkan Metode Pembayaran :\n1. Dana\n2. OVO");
                            pilih = input.nextInt();
                            if (pilih == 1) {
                                System.out.println("Masukkan nomor Dana : ");
                                Dana.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Kata Sandi : ");
                                Dana.setsandi(inputan.nextLine());
                                produk.PulsaDana(50000);
                            } else if (pilih == 2) {
                                System.out.println("Masukkan Nomor OVO : ");
                                OVO.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Sandi : ");
                                OVO.setsandi(inputan.nextLine());
                                produk.PulsaOvo(50000);
                            }
                            produk.info();
                            break;

                        case 5:
                            System.out.println("Masukkan Metode Pembayaran :\n1. Dana\n2. OVO");
                            pilih = input.nextInt();
                            if (pilih == 1) {
                                System.out.println("Masukkan nomor Dana : ");
                                Dana.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Kata Sandi : ");
                                Dana.setsandi(inputan.nextLine());
                                produk.PulsaDana(75000);
                            } else if (pilih == 2) {
                                System.out.println("Masukkan Nomor OVO : ");
                                OVO.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Sandi : ");
                                OVO.setsandi(inputan.nextLine());
                                produk.PulsaOvo(75000);
                            }
                            produk.info();
                            break;
                        case 6:
                            System.out.println("Masukkan Metode Pembayaran :\n1. Dana\n2. OVO");
                            pilih = input.nextInt();
                            if (pilih == 1) {
                                System.out.println("Masukkan nomor Dana : ");
                                Dana.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Kata Sandi : ");
                                Dana.setsandi(inputan.nextLine());
                                produk.PulsaDana(100000);
                            } else if (pilih == 2) {
                                System.out.println("Masukkan Nomor OVO : ");
                                OVO.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Sandi : ");
                                OVO.setsandi(inputan.nextLine());
                                produk.PulsaOvo(100000);
                            }
                            produk.info();
                            break;
                        case 7:
                            System.out.println("Masukkan Metode Pembayaran :\n1. Dana\n2. OVO");
                            pilih = input.nextInt();
                            if (pilih == 1) {
                                System.out.println("Masukkan nomor Dana : ");
                                Dana.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Kata Sandi : ");
                                Dana.setsandi(inputan.nextLine());
                                produk.PulsaDana(150000);
                            } else if (pilih == 2) {
                                System.out.println("Masukkan Nomor OVO : ");
                                OVO.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Sandi : ");
                                OVO.setsandi(inputan.nextLine());
                                produk.PulsaOvo(1500000);
                            }
                            produk.info();
                            break;

                        default:
                            System.out.println("sistem error");
                            System.exit(0);
                    }
                } else if (ops == 2) {
                    System.out.println("Masukkan paket data yang diinginkan   : ");
                    System.out.println("1. 1 GB\t 4 ribu");
                    System.out.println("2. 2 GB\t 5 ribu");
                    System.out.println("3. 5 GB\t 8 ribu");
                    System.out.println("4. 10 GB\t 13 ribu");
                    System.out.println("5. 20 GB\t 23 ribu");
                    System.out.println("6. 50 GB\t 53 ribu");
                    System.out.println("7. 100 GB\t 103 ribu");
                    int metod;
                    int milih = input.nextInt();
                    switch (milih) {
                        case 1:
                            System.out.println("Masukkan Metode Pembayaran :\n1. Dana\n2. OVO\n3. Pulsa");
                            metod = input.nextInt();
                            if (metod == 1) {
                                System.out.println("Masukkan nomor Dana : ");
                                Dana.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Kata Sandi : ");
                                Dana.setsandi(inputan.nextLine());
                                produk.DataDana(1000);
                            } else if (metod == 2) {
                                System.out.println("Masukkan Nomor OVO : ");
                                OVO.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Sandi : ");
                                OVO.setsandi(inputan.nextLine());
                                produk.DataOvo(1000);
                            } else if (metod == 3) {
                                produk.datapulsa(1);
                            }
                            produk.info();
                            break;

                        case 2:
                            System.out.println("Masukkan Metode Pembayaran :\n1. Dana\n2. OVO\n3. Pulsa");
                            metod = input.nextInt();
                            if (metod == 1) {
                                System.out.println("Masukkan nomor Dana : ");
                                Dana.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Kata Sandi : ");
                                Dana.setsandi(inputan.nextLine());
                                produk.DataDana(2000);
                            } else if (metod == 2) {
                                System.out.println("Masukkan Nomor OVO : ");
                                OVO.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Sandi : ");
                                OVO.setsandi(inputan.nextLine());
                                produk.DataOvo(2000);
                            } else if (metod == 3) {
                                produk.datapulsa(2);
                            }
                            produk.info();
                            break;

                        case 3:
                            System.out.println("Masukkan Metode Pembayaran :\n1. Dana\n2. OVO\n3. Pulsa");
                            metod = input.nextInt();
                            if (metod == 1) {
                                System.out.println("Masukkan nomor Dana : ");
                                Dana.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Kata Sandi : ");
                                Dana.setsandi(inputan.nextLine());
                                produk.DataDana(5000);
                            } else if (metod == 2) {
                                System.out.println("Masukkan Nomor OVO : ");
                                OVO.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Sandi : ");
                                OVO.setsandi(inputan.nextLine());
                                produk.DataOvo(5000);
                            } else if (metod == 3) {
                                produk.datapulsa(5);
                            }
                            produk.info();
                            break;

                        case 4:
                            System.out.println("Masukkan Metode Pembayaran :\n1. Dana\n2. OVO\n3. Pulsa");
                            metod = input.nextInt();
                            if (metod == 1) {
                                System.out.println("Masukkan nomor Dana : ");
                                Dana.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Kata Sandi : ");
                                Dana.setsandi(inputan.nextLine());
                                produk.DataDana(10000);
                            } else if (metod == 2) {
                                System.out.println("Masukkan Nomor OVO : ");
                                OVO.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Sandi : ");
                                OVO.setsandi(inputan.nextLine());
                                produk.DataOvo(10000);
                            } else if (metod == 3) {
                                produk.datapulsa(10);
                            }
                            produk.info();
                            break;

                        case 5:
                            System.out.println("Masukkan Metode Pembayaran :\n1. Dana\n2. OVO\n3. Pulsa");
                            metod = input.nextInt();
                            if (metod == 1) {
                                System.out.println("Masukkan nomor Dana : ");
                                Dana.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Kata Sandi : ");
                                Dana.setsandi(inputan.nextLine());
                                produk.DataDana(20000);
                            } else if (metod == 2) {
                                System.out.println("Masukkan Nomor OVO : ");
                                OVO.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Sandi : ");
                                OVO.setsandi(inputan.nextLine());
                                produk.DataOvo(20000);
                            } else if (metod == 3) {
                                produk.datapulsa(20);
                            }
                            produk.info();
                            break;

                        case 6:
                            System.out.println("Masukkan Metode Pembayaran :\n1. Dana\n2. OVO\n3. Pulsa");
                            metod = input.nextInt();
                            if (metod == 1) {
                                System.out.println("Masukkan nomor Dana : ");
                                Dana.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Kata Sandi : ");
                                Dana.setsandi(inputan.nextLine());
                                produk.datapulsa(50000);
                            } else if (metod == 2) {
                                System.out.println("Masukkan Nomor OVO : ");
                                OVO.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Sandi : ");
                                OVO.setsandi(inputan.nextLine());
                                produk.DataOvo(50000);
                            } else if (metod == 3) {
                                produk.datapulsa(50);
                            }
                            produk.info();
                            break;

                        case 7:
                            System.out.println("Masukkan Metode Pembayaran :\n1. Dana\n2. OVO\n3. Pulsa");
                            metod = input.nextInt();
                            if (metod == 1) {
                                System.out.println("Masukkan nomor Dana : ");
                                Dana.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Kata Sandi : ");
                                Dana.setsandi(inputan.nextLine());
                                produk.DataDana(100000);
                            } else if (metod == 2) {
                                System.out.println("Masukkan Nomor OVO : ");
                                OVO.setnomor(inputan.nextLine());
                                System.out.println("Masukkan Sandi : ");
                                OVO.setsandi(inputan.nextLine());
                                produk.DataOvo(100000);
                            } else if (metod == 3) {
                                produk.datapulsa(100);
                            }
                            produk.info();
                            break;

                        default:
                            System.out.println("sistem error");
                            System.exit(0);
                    }
                }
            } else if (opsi == 2) {
                System.out.println("Masukkan Komentar : ");
                Scanner komen = new Scanner(System.in);
                String komentar = komen.nextLine();
                System.out.println("Komentar Anda :\n " + komentar);
                System.out.println("Masukkan Penilaian (1/5)");
                int bintang = input.nextInt();
                if (bintang >= 1) {
                    if (bintang <= 5) {
                        System.out.println("Terima Kasih atas Penilaiannya");
                    }
                } else {
                    System.out.println("salah");
                }
            } else if (opsi == 3) {
                telko.promo();
            } else {
                System.out.println("error");
            }
            System.out.println("Ingin Mengulang: \nY\tN");
            ulg = input.next().charAt(0);
        } while (ulg == 'Y');
    }
}```

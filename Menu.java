
import java.util.*;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Hashtable;

/*membuat abstrak dan menmasukkan methode menu di dalamnya*/
abstract class telkomsel {
    abstract void menu();
}

/* interface promo */
interface promo {
    public void promo();
}

/*
 * membuat methode registrasi dengan turunan dari abstrak class sehingga membuat
 * inheritance
 */
class info extends telkomsel implements promo {
    /* polymorphism */
    public void menu() {
        System.out.println("===============================");
        System.out.println("SELAMAT DATANG DI TELKOMSEL");
        System.out.println("===============================");
        System.out.println("1. BELI PULSA/KUOTA");
        System.out.println("2. KRITIK DAN SARAN ");
        System.out.println("3. PROMO ");
        System.out.println("===============================");
    }

    public void promo() {
        Hashtable<String, Integer> Promo = new Hashtable<String, Integer>();

        Promo.put("1. 30 GB/1 bulan = 120000", 1);
        Promo.put("2. 25 GB/1 bulan = 100000", 2);
        Promo.put("3. 15 GB/1 bulan = 90000", 3);
        Promo.put("4. 10 GB/1 bulan = 70000", 4);
        Promo.put("5. 10 GB/1 minggu = 120000", 5);
        Promo.put("6. 8 GB/1 minggu = 50000", 6);
        Promo.put("7. 5 GB/5 hari = 20000", 7);
        Promo.put("8. 2 GB/3 hari = 10000", 8);
        Promo.put("9. 1 GB/1 hari = 5000", 9);
        Promo.put("10. 500 MB/1 hari = 2500", 10);
        System.out.println("Isi Promo :");
        for (String kunci : Promo.keySet()) {
            int nilaipromo = Promo.get(kunci);
            System.out.println(kunci + " = " + nilaipromo);
        }
        System.out.println("Masukkan nilai kata Kunci : ");
        Scanner masukan = new Scanner(System.in);
        int nilai = masukan.nextInt();
        if (Promo.containsKey(nilai)) {
            nilai = Promo.get(nilai);
        }

        // Menampilkan hasil pencarian
        if (nilai != -1) {
            System.out.println("Nilai " + nilai + " = ditemukan");
        } else {
            System.out.println("Keyword " + nilai + " tidak ditemukan.");
        }
    }

    public void registrasi() {
        Scanner input2 = new Scanner(System.in);

        System.out.println("Selamat datang di Aplikasi Telkomsel");
        System.out.println("Silakan lengkapi data berikut untuk melakukan registrasi:");

        System.out.print("Nickname: ");
        String nama = input2.nextLine();

        System.out.println("Masukkan nomor telepon:");
        String phoneNumber = input2.nextLine();
        Pattern pattern = Pattern.compile("^(\\+62|0)[2-9][0-9]{8,10}$");
        Matcher matcher = pattern.matcher(phoneNumber);
        if (matcher.matches()) {
            System.out.println("Nomor telepon valid.");
        } else {
            System.out.println("Nomor telepon tidak valid.");
        }

        System.out.print("Email: ");
        String email = input2.nextLine();

        System.out.print("Kata sandi: ");
        String kataSandi = input2.nextLine();

        System.out.print("Ulangi kata sandi: ");
        String ulangiKataSandi = input2.nextLine();

        if (kataSandi.equals(ulangiKataSandi)) {
            System.out.println("Registrasi berhasil!");
            System.out.println("Terima kasih " + nama + " telah bergabung dengan Aplikasi Telkomsel.");
        } else {
            System.out.println("Kata sandi yang Anda masukkan tidak sama.");
            System.exit(0);
        }
    }
}

class Pembelian {
    double saldo;
    double hargaPulsa;
    double hargaPaketData;
    double totalpulsa;
    double totaldata;

    public Pembelian(double saldo, double hargaPulsa, double hargaPaketData, double totalpulsa, double totaldata) {
        this.saldo = saldo;
        this.hargaPulsa = hargaPulsa;
        this.hargaPaketData = hargaPaketData;
        this.totalpulsa = totalpulsa;
        this.totaldata = totaldata;
    }

    public void beliPulsa(double jumlah) {
        double totalHarga = jumlah * hargaPulsa;
        if (totalHarga > saldo) {
            System.out.println("Saldo tidak mencukupi.");
        } else {
            saldo -= totalHarga;
            System.out.println("Pulsa sebesar " + jumlah + " berhasil dibeli.");
            System.out.println("Sisa saldo: " + saldo);
            totalpulsa += jumlah;
        }
    }

    public void beliPaketData(double jumlah) {
        double totalHarga = jumlah * hargaPaketData;
        if (totalHarga > saldo) {
            System.out.println("Saldo tidak mencukupi.");
        } else {
            saldo -= totalHarga;
            System.out.println("Paket data sebesar " + jumlah + " GB berhasil dibeli.");
            System.out.println("Sisa saldo: " + saldo);
            totaldata += jumlah;
        }
    }

    public void info() {
        System.out.println("sisa uang = " + saldo);
        System.out.println("jumlah pulsa saat ini = " + totalpulsa);
        System.out.println("jumlah paket data saat ini = " + totaldata);
    }
}

/* inheritance dan ekapsulasi */
public abstract class komen {
    private String kata;
    private int nilai;

    public void setKata(String kata) {
        this.kata = kata;
    }

    public void setnilai(int nilai) {
        this.nilai = nilai;
    }

    public String getkata() {
        return kata;
    }

    public double getnilai() {
        return nilai;
    }

    public void beriSaran(String saran) {
    };

    public void beriRating(int rating) {
    };

}

/* polymorphism */
public class Menilai extends komen {
    @Override
    public void beriRating(int rating) {
        setnilai(rating);
        System.out.println("Rating untuk Telkomsel " + getnilai() + ": ");
    }

    public void beriSaran(String saran) {
        setKata(saran);
        System.out.println("Masukkan Saran " + getkata() + ": ");
    }
}

public class Review extends komen {
    @Override
    public void beriRating(int rating) {
        setnilai(rating);
        System.out.println("Review  " + getnilai() + ": ");
    }

    public void beriSaran(String saran) {
        setKata(saran);
        System.out.println("Nilai " + getkata() + ": ");
    }
}

public class Telkomtsel {
    public static void main(String[] args) {
        info telko = new info();
        Scanner input = new Scanner(System.in);
        Menilai menilai = new Menilai();
        Review review = new Review();

        double saldo = 50000;
        double hargaPulsa = 1000;
        double hargaPaketData = 5000;
        double totalpulsa = 100000;
        double totaldata = 23;
        Pembelian produk = new Pembelian(saldo, hargaPulsa, hargaPaketData, totalpulsa, totaldata);
        char ulg = 'Y';
        telko.registrasi();
        do {
            telko.menu();
            produk.info();
            System.out.println("masukkan opsi   : ");
            int opsi = input.nextInt();
            if (opsi == 1) {
                System.out.println("1. Pulsa \n2. Paket Data");
                int ops = input.nextInt();
                if (ops == 1) {
                    System.out.println("Masukkan jumlah pulsa yang diinginkan   : ");
                    double jumlahPulsa = input.nextDouble();
                    produk.beliPulsa(jumlahPulsa);
                    produk.info();
                } else if (ops == 2) {
                    System.out.println("Masukkan data yang diinginkan   : ");
                    double jumlahPaketData = input.nextDouble();
                    produk.beliPaketData(jumlahPaketData);
                    produk.info();
                }
            } else if (opsi == 2) {
                System.out.println("1. Ulasan\n2. Review");
                int opsii = input.nextInt();
                if (opsii == 1) {
                    System.out.println("Masukkan nilai (0/5) :");
                    menilai.setnilai(input.nextInt());
                    System.out.println("Masukkan masukan :");
                    menilai.setKata(input.nextLine());
                } else if (opsii == 2) {
                    System.out.println("Masukkan nilai untuk review (0/5) :");
                    review.setnilai(input.nextInt());
                    System.out.println("Masukkan saran review : ");
                    review.setKata(input.nextLine());
                }
            } else if (opsi == 3) {
                telko.promo();
            } else {
                System.out.println("error");
            }
            System.out.println("Ingin Mengulang: \nY\tN");
            ulg = input.next().charAt(0);
        } while (ulg == 'Y');
    }
}